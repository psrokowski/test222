﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Model
{
    public class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Model;

namespace Test
{
    public interface IPeopleService
    {
        List<Person> GetPeople();

        void AddPerson(Person person);
    }
}

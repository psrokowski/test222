﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Model;

namespace Test.Services
{
    public class PeopleService : IPeopleService
    {

        List<Person> People = new List<Person>() { new Person() {Name ="Person1", Surname ="Person1" }, { new Person() {Name= "Person2", Surname = "Person2" } } };
        public void AddPerson(Person person) => People.Add(person);

        public List<Person> GetPeople() => People;
    }
}